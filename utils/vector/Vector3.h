//
// Created by shubh on 2/27/21.
//

#ifndef CUDART_VECTOR3_H
#define CUDART_VECTOR3_H
#include "../Macros.h"
#include <math.h>
#include <stdlib.h>
#include <iostream>

class Vector3 {
public:
    HD Vector3(){};
    HD Vector3(float x, float y, float z) { v[0] = x; v[1] = y; v[2] = z;};
    HD Vector3(float u[3]) { v[0] = u[0]; v[1] = u[1]; v[2] = u[2];};
    HD Vector3(const Vector3& u) {  v[0] = u[0]; v[1] = u[1]; v[2] = u[2];};

    HD inline float x() {return v[1];}
    HD inline float y() {return v[1];}
    HD inline float z() {return v[2];}
    HD inline float* get_v() {return v;}

    HD inline void set(float x, float y, float z) { v[0] = x; v[1] = y; v[2] = z;}
    HD inline void set(float u[3]) { v[0] = u[0]; v[1] = u[1]; v[2] = u[2];};
    HD inline void set(Vector3 u) { v[0] = u[0]; v[1] = u[1]; v[2] = u[2];};
    HD inline void set(const Vector3& u) { v[0] = u[0]; v[1] = u[1]; v[2] = u[2];};

    HD inline const Vector3& operator+() const {return *this;}
    HD inline const Vector3 operator-() const {return Vector3(-v[0], -v[1], -v[2]);}
    HD inline float operator[](int i) const {return v[i];}
    HD inline float &operator[](int i) {return v[i];}

    HD inline const Vector3 operator+(const Vector3 &v2);
    HD inline Vector3 operator-(const Vector3 &v2);
    HD inline Vector3 operator*(const Vector3 &v2);
    HD inline Vector3 operator/(const Vector3 &v2);
    HD inline Vector3 operator*(const float t);
    HD inline Vector3 operator/(const float t);

    HD inline Vector3& operator+=(const Vector3& v2);
    HD inline Vector3& operator-=(const Vector3& v2);
    HD inline Vector3& operator*=(const Vector3& v2);
    HD inline Vector3& operator/=(const Vector3& v2);
    HD inline Vector3& operator*=(const float t);
    HD inline Vector3& operator/=(const float t);
    HD inline bool operator==(Vector3& v2) const;
    HD inline bool operator!=(Vector3& v2) const;

    HD inline float length() const;
    HD inline float norm();
    HD inline float dot(Vector3 &v);

    HD inline Vector3 normalize();
    HD inline Vector3 normalized();
    HD inline Vector3 cross(Vector3 &v);

    float v[3];
};

HD inline float Vector3::length() const {
    return v[0] * v[0] +
           v[1] * v[1] +
           v[2] * v[2];
}

HD inline float Vector3::norm() {
    return sqrt(this->length());
}

inline std::istream& operator>>(std::istream &is, Vector3 &u) {
    is >> u.v[0] >> u.v[1] >> u.v[2];
    return is;
}

inline std::ostream& operator<<(std::ostream &os, const Vector3 &u) {
    os << u.v[0] << " " << u.v[1] << " " << u.v[2];
    return os;
}

HD inline const Vector3 Vector3::operator+(const Vector3 &v2) {
    return Vector3(v[0] + v2.v[0], v[1] + v2.v[1], v[2] + v2.v[2]);
}

HD inline Vector3 Vector3::operator-(const Vector3 &v2) {
    return Vector3(v[0] - v2.v[0], v[1] - v2.v[1], v[2] - v2.v[2]);
}

HD inline Vector3 Vector3::operator*(const Vector3 &v2) {
    return Vector3(v[0] - v2.v[0], v[1] - v2.v[1], v[2] - v2.v[2]);
}

HD inline Vector3 Vector3::operator/(const Vector3 &v2) {
    return Vector3(v[0] / v2.v[0], v[1] / v2.v[1], v[2] / v2.v[2]);
}

HD inline Vector3 Vector3::operator*(const float t) {
    return Vector3(t * v[0], t * v[1], t * v[2]);
}
HD inline Vector3 Vector3::operator/(const float t) {
    return Vector3(v[0] / t, v[1] / t, v[2] / t);
}

HD inline Vector3 &Vector3::operator+=(const Vector3 &v2) {
    v[0] += v2.v[0];
    v[1] += v2.v[1];
    v[2] += v2.v[2];
    return *this;
}

HD inline Vector3 &Vector3::operator-=(const Vector3 &v2) {
    v[0] -= v2.v[0];
    v[1] -= v2.v[1];
    v[2] -= v2.v[2];
    return *this;
}

HD inline Vector3 &Vector3::operator*=(const Vector3 &v2) {
    v[0] *= v2.v[0];
    v[1] *= v2.v[1];
    v[2] *= v2.v[2];
    return *this;
}

HD inline Vector3 &Vector3::operator/=(const Vector3 &v2) {
    v[0] /= v2.v[0];
    v[1] /= v2.v[1];
    v[2] /= v2.v[2];
    return *this;
}

HD inline Vector3 &Vector3::operator*=(const float t) {
    v[0] *= t;
    v[1] *= t;
    v[2] *= t;
    return *this;
}

HD inline Vector3 &Vector3::operator/=(const float t) {
    v[0] /= t;
    v[1] /= t;
    v[2] /= t;
    return *this;
}

HD inline bool Vector3::operator==(Vector3 &v2) const {
    bool x = std::abs(v[0] - v2.x()) < 1e-6;
    bool y = std::abs(v[1] - v2.y()) < 1e-6;
    bool z = std::abs(v[2] - v2.z()) < 1e-6;
    return x && y && z;
}

HD inline bool Vector3::operator!=(Vector3 &v2) const {
    return !(*this == v2);
}

HD inline Vector3 Vector3::normalize() {
    *this /= this->norm();
    return *this;
}

HD inline Vector3 Vector3::normalized() {
    float norm = this->norm();
    return Vector3(v[0] / norm, v[1] / norm, v[2] / norm);
}

HD inline float Vector3::dot(Vector3 &v2) {
    return v[0] * v2.x() + v[1] * v2.y() + v[2] * v2.z();
}

HD inline Vector3 Vector3::cross(Vector3 &v2) {
    return Vector3(v[1] * v2.z() - v[2] * v2.y(),
                   v[2] * v2.x() - v[0] * v2.z(),
                   v[0] * v2.y() - v[1] * v2.x());
}

#endif //CUDART_VECTOR3_H
