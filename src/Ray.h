//
// Created by shubh on 2/27/21.
//

#ifndef CUDART_RAY_H
#define CUDART_RAY_H

#include "../utils/vector/Vector3.h"

class Ray
{
    public:
        DV Ray() {}
        DV Ray(const Vector3& a, const Vector3& b) { ori = a; dir = b; }
        DV Vector3 origin() const { return ori; }
        DV Vector3 direction() const { return dir; }
        DV Vector3 displace(float t) const { return ori + t*dir; }

        Vector3 ori;
        Vector3 dir;
};

#endif //CUDART_RAY_H
