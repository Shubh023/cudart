#!/bin/sh


mode=$1
if [ $# != 0 ]
then
    if [ $mode == "make" ]
    then
        echo "Creating cmake-build-debug"
    	mkdir cmake-build-debug
 	cd cmake-build-debug
	cmake ../
    	make
    	echo "Check cmake-build-debug the executable is ready"
    elif [ $mode == "clean" ]
    then
    	echo "Cleaning up cmake-build-debug..."
	rm -rf cmake-build-debug
        echo "Done Cleaning up."
    fi
else
    echo "Creating cmake-build-debug"
    mkdir cmake-build-debug
    cd cmake-build-debug
    cmake ../
    make
    echo "Check cmake-build-debug the executable is ready"
fi

