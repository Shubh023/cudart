//
// Created by shubh on 2/27/21.
//

#ifndef CUDART_MACROS_H
#define CUDART_MACROS_H

#define HD __host__ __device__
#define HT __host__
#define DV __device__
#define GBL __global__

#endif //CUDART_MACROS_H
